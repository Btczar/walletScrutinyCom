---
wsId: AdmiralMarkets
title: 'Admirals: Trade CFDs & Stocks'
altTitle: 
authors:
- danny
appId: com.admiralmarkets.tradersroom
appCountry: us
idd: 1222861799
released: 2017-06-28
updated: 2022-05-27
version: 5.10.0
stars: 4.8
reviews: 17
size: '61735936'
website: https://admiralmarkets.com/
repository: 
issue: 
icon: com.admiralmarkets.tradersroom.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive: 
twitter: AdmiralsGlobal
social:
- https://www.linkedin.com/company/-admiral-markets-group
- https://www.facebook.com/AdmiralsGlobal

---

{% include copyFromAndroid.html %}

