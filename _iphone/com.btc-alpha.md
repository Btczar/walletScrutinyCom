---
wsId: BTCAlpha
title: 'BTC-Alpha: Buy Sell Bitcoin'
altTitle: 
authors:
- danny
appId: com.btc-alpha
appCountry: us
idd: 1437629304
released: 2019-04-20
updated: 2022-05-28
version: 1.13.2
stars: 4.3
reviews: 11
size: '97747968'
website: https://btc-alpha.com
repository: 
issue: 
icon: com.btc-alpha.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: btcalpha
social:
- https://www.linkedin.com/company/btcalpha
- https://www.facebook.com/btcalpha

---

{% include copyFromAndroid.html %}


